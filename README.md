# ESTADÍSTICA PARA INGENIERÍA Y CIENCIAS
Resolución de las prácticas del curso 'Estadística para Ingeniería y Ciencias' (2022)
## Herramientas
* R
* JupyterLab
## Contenidos
* [PRÁCTICA 1: Simulación](https://github.com/ivan-svetlich/statistics-r/blob/main/notebooks/TP1.ipynb)
* [PRÁCTICA 2: Prueba Ji-cuadrado, Tablas de Contingencia, Bondad de Ajuste](https://github.com/ivan-svetlich/statistics-r/blob/main/notebooks/TP2.ipynb)
